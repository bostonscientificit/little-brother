
.PHONY: extract
extract: 
	python extract_embeddings.py --dataset dataset \
		--embeddings output/embeddings.pickle \
		--detector face_detection_model \
		--embedding-model openface_nn4.small2.v1.t7

.PHONY: train
train: 
	python3 train_model.py --embeddings output/embeddings.pickle --recognizer output/recognizer.pickle --le output/le.pickle

.PHONY: model
model: extract train

.PHONY: video
video: 
	python recognize_video.py --detector face_detection_model \
        --embedding-model openface_nn4.small2.v1.t7 \
        --recognizer output/recognizer.pickle \
        --le output/le.pickle

.PHONY: image
image: 
	python recognize.py --detector face_detection_model \      
		--embedding-model openface_nn4.small2.v1.t7 \
		--recognizer output/recognizer.pickle \
		--le output/le.pickle \
		--image images/adrian.jpg

