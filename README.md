
# Little Brother

Detailed steps: https://www.pyimagesearch.com/2018/09/24/opencv-face-recognition/

## Install 

```
pip install --upgrade imutils
pip install scikit-learn
brew install opencv3
```


## Usage

```bash
# Extract embeddings from face dataset
python extract_embeddings.py --dataset dataset --embeddings output/embeddings.pickle --detector face_detection_model --embedding-model openface_nn4.small2.v1.t7

# train
python train_model.py --embeddings output/embeddings.pickle --recognizer output/recognizer.pickle --le output/le.pickle

# image
python recognize.py --detector face_detection_model --embedding-model openface_nn4.small2.v1.t7 --recognizer output/recognizer.pickle --le output/le.pickle --image images/adrian.jpg

# video
python recognize_video.py --detector face_detection_model --embedding-model openface_nn4.small2.v1.t7 --recognizer output/recognizer.pickle --le output/le.pickle  
```

